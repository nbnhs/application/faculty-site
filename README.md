# NHS Application Suite

## Faculty Site

This site is what the faculty (teachers) see.

## Set It Up

`faculty-site` is designed to work with GitLab Pages, and so this guide will follow a traditional GitLab Pages set-up.

### Environment Variables

The customizable parts of this code are controlled through environment variables. All the necessary ones are found in `src/vars.ts`, shown below.

```
export const SUPPORT_EMAIL = "hello@example.com"
export const RELAY_DOMAIN = "relay.example.com"
```

* `SUPPORT_EMAIL` holds the email users should contact if they're having issues with the site.
* `RELAY_DOMAIN` is the domain of the [database relay](https://gitlab.com/nbnhs/application/database-relay).

### Building

`make` builds the site, while `make test` builds *and* opens it in your default web browser.