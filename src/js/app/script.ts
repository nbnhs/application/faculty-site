import { RELAY_DOMAIN } from "../vars";
import getCookie from "./getCookie";

/**
 * Uploads the provided image to the database.
 *
 * @param format - format of the image (e.g. image/png)
 * @param imageData - data of the image, preferrably a blob
 * @param ID - ID of the Applicant to attach the image to
 * @returns A (promise for a) string containing the response from the database relay.
 */
async function uploadImageFile(
  format: string,
  imageData: any,
  ID: string,
): Promise<string> {
  console.log("Uploading image...");
  const url = `https://${RELAY_DOMAIN}`;
  const customHeaders = new Headers();
  customHeaders.append("Content-Type", format);
  customHeaders.append("Content-Length", String(imageData.length));
  customHeaders.append("X-NHS-ID", ID);
  customHeaders.append("X-NHS-ImageKind", "teacher");

  const response = await fetch(url, {
    method: "POST",
    headers: customHeaders,
    body: imageData,
  });

  if (!response.ok) {
    throw new Error(`Invalid response! Couldn't upload image for ${ID}.`);
  }
  console.log("Uploaded.");
  return await response.text();
}

window.onload = () => {
  document.getElementById("faculty-name").innerHTML = getCookie(
    "faculty-name",
  ).split(`"`)[1];

  document.querySelector("#btn-uploadsubmit").addEventListener("click", () => {
    // Please wait... text
    (document.querySelector(
      "#btn-uploadsubmit",
    ) as HTMLButtonElement).innerText = "Please wait...";
    const input = (document.getElementById("upload") as HTMLInputElement)
      .files[0];
    const id = (document.getElementById("id-card-number") as HTMLInputElement)
      .value;

    uploadImageFile(input.type, input, id).then((val: string) => {
      console.log(val);
      alert("Success!");
      (document.querySelector(
        "#btn-uploadsubmit",
      ) as HTMLButtonElement).innerText = "Thank You!";
    }).catch((failure: string) => {
      alert(`Error uploading image. Got error ${failure}.`);
      (document.querySelector(
        "#btn-uploadsubmit",
      ) as HTMLButtonElement).innerText = "Try Again?";
    });
  });
};
