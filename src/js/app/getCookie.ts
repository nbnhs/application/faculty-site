/**
 * Retrieves a cookie from "document.cookie".
 * Thank you to {@link https://gist.github.com/hunan-rostomyan/28e8702c1cecff41f7fe64345b76f2ca | this guy}!
 * @param name - name of the cookie to retrieve
 */
export default function getCookie(name: string): string {
  const nameLenPlus = name.length + 1;
  return (
    document.cookie
      .split(";")
      .map((c) => c.trim())
      .filter((cookie) => {
        return cookie.substring(0, nameLenPlus) === `${name}=`;
      })
      .map((cookie) => {
        return decodeURIComponent(cookie.substring(nameLenPlus));
      })[0] || null
  );
}
