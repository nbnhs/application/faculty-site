import { RELAY_DOMAIN, SUPPORT_EMAIL } from "./vars";

/**
 * Gets the token used by the user to login.
 * @returns the token used by the user to login (e.g. index.html?token=5) => 5)
 */
function getLoginToken(): string {
  const query = window.location.search.substring(1);
  const vars = query.split("&");
  for (const variable of vars) {
    const pair = variable.split("=");
    if (decodeURIComponent(pair[0]) === "token") {
      return decodeURIComponent(pair[1]);
    }
  }
  alert("Please use the URL you have been provided.");
}

/**
 * Defines how an object created to represent a Faculty member in the database should look.
 */
interface IFacultyDatabaseEntry {
  type: string;
  name: string;
}

window.onload = () => {
  const login = getLoginToken();
  console.log(`Got token ${login}`);
  const resp = fetch(
    `https://${RELAY_DOMAIN}?operation=authfaculty&id=${login}`,
  );
  resp.then((val: Response) => {
    console.log("Got auth response");
    val.text().then((s: string) => {
      console.log(s);
      if (s === "Failure") {
        alert("That URL is invalid.");
      } else {
        const obj: IFacultyDatabaseEntry = JSON.parse(s);
        document.cookie = `faculty-name="${obj.name}"`;
        window.location.replace("app/index.html");
      }
    }).catch((failure: string) => {
      alert(`Login error: ${failure}. Please try again or email ${SUPPORT_EMAIL} if the issue persists.`)
    });
  });
};
